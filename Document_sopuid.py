import os 
import pandas as pd 

with open('train.txt') as f:
    lines = f.readlines()

lines = map(lambda s: s.strip(), lines)

line = []
line1 = []
for i in range(len(lines)):
	line.append(lines[i].split('/')[2])
	line1.append('/nfs/' + lines[i] + '.png')

name = 'Training_sopuids.csv'
df = pd.DataFrame({'Training SOP-UIDs':line, 'Path_to_Images':line1})
#df.to_excel(name,sheet_name = 'sheet1', index = False)
df.to_csv(name,index = False)

path_to_test_images = '/mnt/Array/share/users/anirudh/Images/Normal_and_tommo_slices_test/'
Test_sopuid = [x.split('.png')[0] for x in os.listdir(path_to_test_images)]
path_to_test_xmls = '/nfs/GT/Anirudh_NIpple_FInder_FRCNN_GT/Ground_Truths_maxwell/Normal_and_tommo_slices_test/'
Test_xmls = [y.split('.xml')[0] for y in os.listdir(path_to_test_xmls)]

Test_sopuid = list(set(Test_xmls).intersection(Test_sopuid))


name2 = 'Testing_sopuids.csv'
df = pd.DataFrame({'Testing SOP-UIDs':Test_sopuid})
#df.to_excel(name,sheet_name = 'sheet1', index = False)
df.to_csv(name2,index = False)