import os
import numpy as np
import glob

with open('train.txt') as f:
    lines = f.readlines()

lines = map(lambda s: s.strip(), lines)
#lines = [line.strip('images/PNGs_2048x1600') for line in open('train.txt')]
line = []
for i in range(len(lines)):
	line.append(lines[i].split('/')[2])
#print line
fulline = []
for j in range(len(lines)):
	fulline.append(lines[j])

path_groundtruth = '/mnt/Array/share/users/anirudh/Ground_Truths_maxwell/train/'
xml_files = [os.path.splitext(filename)[0] for filename in os.listdir(path_groundtruth)]
#print images

dictionary = {}
for k in range(len(lines)):
	dictionary[line[k]] = fulline[k]

#print dictionary

A = list(set(line).intersection(xml_files))
final_list = []
for m in range(len(A)):
	final_list.append(dictionary[A[m]])

print len(final_list)

with open('train1.txt','w') as f:
	for i in final_list:
		f.write("%s\n"%i)