import os
import subprocess
import numpy as np


path_to_models = '/nfs/experiments/Anirudh_FRCNN/Experiment3/Models/'#Change this accordingly
DirectoryContents = [os.path.splitext(filename) for filename in os.listdir(path_to_models)]

#print DirectoryContents
names = []
rest = []
for i in range(len(DirectoryContents)):
	names.append(DirectoryContents[i][0])
	rest.append(DirectoryContents[i][1])

names  = list(set(names))

if 'checkpoint' in names: names.remove('checkpoint')

print (names[0].split('.')[1]).split('-')[1]
path_to_inference = '/nfs/experiments/Anirudh_FRCNN/Experiment3/inference/'
for k in range(len(names)):
	path =  path_to_inference + names[k].split('.')[0] + (names[k].split('.')[1]).split('-')[1]
	os.mkdir(path)
	List = 'python ' + 'inference.py ' + '-i /mnt/Array/share/users/anirudh/Images/Normal_and_tommo_slices/ ' + '-o ' + path + '/ ' + '-g 2 ' + '-n 2 ' + ' -c ' + path_to_models + names[k]
	print List
	p = subprocess.Popen(List, bufsize = -1, shell=True)
	p.wait()