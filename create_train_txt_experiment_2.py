import os 
import glob
import numpy as np 


path_to_edge_cases = '/nfs/experiments/Anirudh_FRCNN/Experiment3/extra_train/'
path_to_tommo_slices = '/nfs/experiments/Anirudh_FRCNN/Experiment3/tommo_slices'

path1 = 'experiments/Anirudh_FRCNN/Experiment3/extra_train/'
path2 = 'experiments/Anirudh_FRCNN/Experiment3/tommo_slices/'
edge_cases = [path1 + os.path.splitext(filename)[0] for filename in os.listdir(path_to_edge_cases)]
tommo_slices = [path2 + os.path.splitext(k)[0] for k in os.listdir(path_to_tommo_slices)]


with open('train1.txt') as f:
    lines = f.readlines()

lines = map(lambda s: s.strip(), lines)

final_list = lines + edge_cases + tommo_slices

with open('train_new.txt','w') as f:
	for i in final_list:
		f.write("%s\n"%i)

