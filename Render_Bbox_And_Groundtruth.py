import numpy as np 
import cv2
import matplotlib.pyplot as plt 
import os 
import glob
import json
import xml.etree.ElementTree as ET 
import requests 

#Declare all the paths  
path_to_json = '/nfs/experiments/Anirudh_FRCNN/Experiment2/inference/CM-FRCNN65000/'
#path_to_json = '/nfs/experiments/Anirudh_Nipple_Finder_FRCNN/inference/CM-FRCNN80000/'
#path_to_PNGs = '/nfs/images/PNGs_2048x1600_maxwell/Negatives_for_nipple_finder/'
path_to_PNGs = '/mnt/Array/share/users/anirudh/Images/Normal/'
#path_to_save_rendered_images1 = '/nfs/experiments/Anirudh_Nipple_Finder_FRCNN/Rendered_Negatives/'
#path_to_save_rendered_images = '/nfs/experiments/Anirudh_Nipple_Finder_FRCNN/RenderedImages/'
path_to_GT = '/nfs/GT/Anirudh_NIpple_FInder_FRCNN_GT/Ground_Truths_maxwell/Normal/'

#Reading the xml files to get the files fo which GT is available
#os.chdir(path_to_GT)
xmlfiles = [filename for filename in os.listdir(path_to_GT)]

presedntDir = os.getcwd()
os.chdir(path_to_json)
jsonfiles = [filename for filename in os.listdir(path_to_json)]
print jsonfiles[0]
#Parse the json and create a dictionary 
json_filenames = {}
for j in range(len(jsonfiles)):
	try:
		if (jsonfiles[j].replace('json','xml') in xmlfiles): #Keeping only those key values whose GT is available 
			with open(jsonfiles[j]) as json_file:
				info = json.load(json_file)
			json_filenames[jsonfiles[j].replace('json','png')] = info
	except:
		pass
print len(json_filenames.keys())

#Read the json, pick up the corresponding image and render the bounding box along with the maximum score on it (Score also needs to be greater than 0.01)
PNGfilename = [x for x in os.listdir(path_to_PNGs)]
#print PNGfilename

#Make a dictionary to find the key as image name and value as the boundary box co-ordinates
Dict = {}
for k in range(len(json_filenames)):
	variable = (json_filenames.values())[k]
	FRCNN_SCORE = []
	List = []
	for j in range(len(variable)):
		FRCNN_SCORE.append(variable[j][u'frcnnScore'])
		#print len(FRCNN_SCORE)
		max_frcnnScore = max(FRCNN_SCORE)
		index = np.argmax(FRCNN_SCORE)
	xmin = variable[index][u'min'][u'x']
	xmax = variable[index][u'max'][u'x']
	ymin = variable[index][u'min'][u'y']
	ymax = variable[index][u'max'][u'y']
	frcnnScore = variable[index][u'frcnnScore']
	List.append(xmin)
	List.append(ymin)
	List.append(xmax)
	List.append(ymax)
	List.append(frcnnScore)
	Dict[json_filenames.keys()[k]] = List

os.chdir(path_to_GT)
Dict_xml = {}
for m in range(len(xmlfiles)):
	tree = ET.parse(xmlfiles[m]) 
	root = tree.getroot()
	for item in root.findall('./object/bndbox'):
		bbox = {}
		for child in item:
			bbox[child.tag] = child.text.encode('utf8')
	Dict_xml[xmlfiles[m].replace('.xml','.png')] = bbox
#print Dict_xml.keys()

os.chdir(path_to_PNGs)
Images = {}
for images in glob.glob('*.png'):
    Image = cv2.imread(images)
    Images[images] = Image

for key in Dict.keys():
	if key in Images.keys():
		temp = Dict[key]
		temp2 = Dict_xml[key]
		x1 = int(temp[0])
		y1 = int(temp[1])
		x2 = int(temp[2])
		y2 = int(temp[3])
		fs = temp[4]
		xmin_GT = int(temp2['xmin'])
		xmax_GT = int(temp2['xmax'])
		ymax_GT = int(temp2['ymax'])
		ymin_GT	= int(temp2['ymin'])
		#img = cv2.cvtColor(Images[key],cv2.COLOR_GRAY2RGB)
		img = Images[key]
		cv2.rectangle(img,(xmin_GT,ymin_GT),(xmax_GT,ymax_GT),(255,0,0),3)
		cv2.rectangle(img,(x1,y1),(x2,y2),(0,0,255),3)
		cv2.putText(img,'%.02f'%fs,(x1,y1-5),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255),3)
		cv2.imwrite('/nfs/experiments/Anirudh_FRCNN/Experiment2/inference/CM-FRCNN65000/Rendered_Images/%s'%key,img)
		#cv2.imwrite('/nfs/experiments/Anirudh_Nipple_Finder_FRCNN/RenderedImages/%s'%key,img)
#Save the rendered images to a  different location
