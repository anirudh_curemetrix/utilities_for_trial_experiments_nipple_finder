import os
import glob
import numpy as np 
import shutil

#Read .txt file 
with open('train.txt') as f:
    lines = f.readlines()

lines = map(lambda s: s.strip(), lines)
#lines = [line.strip('images/PNGs_2048x1600') for line in open('train.txt')]
line = []
for i in range(len(lines)):
	line.append(lines[i].split('/')[2])
#print line


#list(set(list1).intersection(list2))
path1 = '/nfs/images/PNGs_2048x1600/'
images1 = [os.path.splitext(filename)[0] for filename in os.listdir(path1)]

path2 = '/nfs/images/PNGs_2048x1600_CBIS-DDSM_Mass_exp2/'
images2 = [os.path.splitext(filename)[0] for filename in os.listdir(path2)]


A = list(set(images1).intersection(line))
os.chdir(path1)
A1 = [A[i]+'.png' for i in range(len(A))]
#for f in A1:
#	shutil.copy(f,'/mnt/Array/share/users/anirudh/Images/train')

B = list(set(images2).intersection(line))
B1 = [B[i]+'.png' for i in range(len(B))]
os.chdir(path2)
for k in B1:
	shutil.copy(k,'/mnt/Array/share/users/anirudh/Images/train')


C = A + B
print(len(C))
print A1