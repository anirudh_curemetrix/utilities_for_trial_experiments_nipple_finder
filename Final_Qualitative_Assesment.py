import os
import numpy as np
import xml.etree.ElementTree as ET 
import requests 
import json
import argparse
from pandas import DataFrame
import matplotlib.pyplot as plt
from sklearn import metrics

def FindROICenter(jsondictionary,threshold):#Function for finding ROI Center
	m = threshold
	json_filenames = jsondictionary
	length_dictionary = len(json_filenames)
	#print length_dictionary
	outsideDict = {}
	for j in range(length_dictionary):
		var = list(json_filenames.values())[j]
		insideDict = []
		max_frcnnScore = 0
		for k in range(len(var)):
			if ((var[k][u'frcnnScore'] >= m) and (var[k][u'frcnnScore'] > max_frcnnScore)): 
				max_frcnnScore = var[k][u'frcnnScore'] 
				xmax = var[k][u'max'][u'x']
				ymax = var[k][u'max'][u'y']
				xmin = var[k][u'min'][u'x']
				ymin = var[k][u'min'][u'y']
				ROICenter_x = (xmax + xmin)/2
				ROICenter_y = (ymax + ymin)/2
				insideDict.append(ROICenter_x)
				insideDict.append(ROICenter_y)
		outsideDict[list(json_filenames.keys())[j]] = insideDict
	#print outsideDict.values()
	return outsideDict

def compare(xmin_GT,xmax_GT,ymin_GT,ymax_GT,ROICenter,true_positive,false_positive):
	if ((xmin_GT < ROICenter[0] < xmax_GT) and (ymin_GT < ROICenter[1] < ymax_GT)):
		true_positive = true_positive + 1
	else: 
		false_positive = false_positive + 1
	return true_positive,false_positive

def Find_Thresholds(jsondictionary):
	length_dictionary = len(jsondictionary)
	thresholds = []
	for j in range(len(jsondictionary)):
		variable = list(jsondictionary.values())[j]
		for k in range(len(variable)):
			if (variable[k][u'frcnnScore'] > 0.01):
				thresholds.append(variable[k][u'frcnnScore'])
	thresholds.sort()
	return thresholds

if __name__=='__main__':
	parser = argparse.ArgumentParser(description='Qualitative Assessment Code')
	# path to jsons  example: /nfs/experiments/Anirudh_Nipple_Finder_FRCNN/inference/ 
	parser.add_argument('-j','--json_path', dest='json_path', help='Path to jsons', required=True) 
	# path to ground truths example: /nfs/GT/Anirudh_NIpple_FInder_FRCNN_GT/Ground_Truths_maxwell/Normal/
	parser.add_argument('-g',  '--groundtruth_path', dest='groundtruth_path', help='Path to ground truths', required=True)
	# Name of excel sheet example:CM-FRCNN-60000.xlsx
	parser.add_argument('-e', '--excelfilename', dest = 'excelfilename', help= 'Name of the excel sheet you want to create', required = True)
	args = parser.parse_args()
	#path_to_json = '/nfs/experiments/Anirudh_Nipple_Finder_FRCNN/inference/'#Path to jsons
	#path_to_GT = '/nfs/GT/Anirudh_NIpple_FInder_FRCNN_GT/Ground_Truths_maxwell/Normal/'#Path to Ground_truths
	path_to_json = args.json_path
	path_to_GT = args.groundtruth_path
	excelFileName = args.excelfilename
	presentdir = os.getcwd()
	os.chdir(path_to_GT)
	files = [filename for filename in os.listdir(path_to_GT)]
	#print files
	print "Lets begin"
	bbox_list = []
	for k in range(len(files)):
		tree = ET.parse(files[k]) 
		root = tree.getroot()
		filename = {}
		for item in root.findall('./object/bndbox'):
			bbox = {}
			for child in item:
				bbox[child.tag] = child.text.encode('utf8')
		filename[files[k]] = bbox
		bbox_list.append(filename)

	#print bbox_list[0].keys()
	
	os.chdir(path_to_json)
	jsonfiles = [filename for filename in os.listdir(path_to_json)]
	json_filenames = {}
	for i in range(len(jsonfiles)):
		try:
			with open(jsonfiles[i]) as json_file:
				data = json.load(json_file)
			json_filenames[jsonfiles[i].replace('.json','.xml')] = data
		except:
			pass

	Final_True_Positive = []
	Final_False_Positive = []
	thresholds = Find_Thresholds(json_filenames)
	for u in range(len(thresholds)):
		Dict = FindROICenter(json_filenames,thresholds[u])
		true_positive = 0
		false_positive = 0
		for j in range(len(bbox_list)):
			for key in bbox_list[j].keys():		
				if key in Dict.keys():
					variable1 = bbox_list[j].values()
					variable2 = variable1[0]
					xmin_GT = int(variable2['xmin'])
					xmax_GT = int(variable2['xmax'])
					ymax_GT = int(variable2['ymax'])
					ymin_GT	= int(variable2['ymin'])
					List = Dict[key]
					if len(List) == 2:
						true_positive,false_positive = compare(xmin_GT,xmax_GT,ymin_GT,ymax_GT,List,true_positive,false_positive)
						#print true_positive
					#if len(List) > 2:
						#chunks = [List[x:x+2] for x in range(0,len(List),2)]
						#for q in range(len(List)/2):
							#true_positive,false_positive = compare(xmin_GT,xmax_GT,ymin_GT,ymax_GT,chunks[q],true_positive,false_positive)
		Final_True_Positive.append(true_positive)
		Final_False_Positive.append(false_positive)
	print(Final_True_Positive)
	print(Final_False_Positive)
	NumOfImages = len(bbox_list)
	df = DataFrame({'True Positives':Final_True_Positive, 'False Positives': Final_False_Positive, 'Number of Images': NumOfImages})
	df.to_excel(excelFileName,sheet_name = 'sheet1', index = False)
	TPR = [x/float(NumOfImages) for x in Final_True_Positive]
	FPPI = [x/float(NumOfImages) for x in Final_False_Positive]
	#TPR = [x/max(TPR) for x in TPR]
	#FPPI = [x/max(FPPI) for x in FPPI]
	a = 0
	FPPI_new = FPPI
	FPPI_new.append(1.0)
	FPPI_new.insert(0,a)
	TPR_new = TPR
	TPR_new.append(max(TPR))
	TPR_new.insert(0,a)
	AUC = metrics.auc(FPPI_new,TPR_new,reorder = True)
	print AUC
	plt.figure(1)
	plt.plot(thresholds,TPR_new[1:len(TPR_new)-1],'-')
	plt.title('TPR VS THRESHOLD')
	plt.xlabel('THRESHOLDS')
	plt.ylabel('TRUE POSITIVE RATE')
	plt.xlim([0.0,1.0])
	plt.ylim([0.0,1.0])
	plt.savefig('TPR_VS_THRESHOLD_{0}.png'.format(excelFileName.split('.')[0]))
	#plt.show()
	plt.figure(2)
	plt.plot(thresholds,FPPI_new[1:len(FPPI_new)-1],'-')
	plt.title('FPPI VS THRESHOLD')
	plt.xlabel('THRESHOLDS')
	plt.ylabel('FALSE POSITIVE PER IMAGE')
	plt.xlim([0.0,1.0])
	plt.ylim([0.0,1.0])
	plt.savefig('FPPI VS THRESHOLD_{0}.png'.format(excelFileName.split('.')[0]))
	#plt.show()
	
	plt.figure(3)
	FPPI_new.sort()
	TPR_new.sort()
	plt.plot(FPPI_new,TPR_new)
	plt.title('TPR VS FPPI')
	plt.xlabel('FALSE POSITIVES PER IMAGE')
	plt.ylabel('TRUE POSITIVE RATE')
	plt.xlim([0.0,1.0])
	plt.ylim([0.0,1.0])
	plt.text(0.5,0.5,'pAUC = {:.4f}'.format(AUC))
	plt.savefig('TPR VS FPPI_{0}.png'.format(excelFileName.split('.')[0]))
	plt.show()